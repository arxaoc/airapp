﻿namespace AirApp
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.hScrollBar1 = new System.Windows.Forms.HScrollBar();
            this.label1 = new System.Windows.Forms.Label();
            this.smoothingText = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.hScrollBar2 = new System.Windows.Forms.HScrollBar();
            this.HistoryLengthText = new System.Windows.Forms.Label();
            this.hScrollBar3 = new System.Windows.Forms.HScrollBar();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.notification_dust10_text = new System.Windows.Forms.Label();
            this.notification_dust10_value = new System.Windows.Forms.HScrollBar();
            this.notification_dust2_text = new System.Windows.Forms.Label();
            this.notification_dust2_value = new System.Windows.Forms.HScrollBar();
            this.notification_dust1_text = new System.Windows.Forms.Label();
            this.notification_dust1_value = new System.Windows.Forms.HScrollBar();
            this.notification_ch2o_text = new System.Windows.Forms.Label();
            this.notification_ch2o_value = new System.Windows.Forms.HScrollBar();
            this.notification_co2_text = new System.Windows.Forms.Label();
            this.notification_co2_value = new System.Windows.Forms.HScrollBar();
            this.smoothedWarnings = new System.Windows.Forms.CheckBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // hScrollBar1
            // 
            this.hScrollBar1.LargeChange = 1;
            this.hScrollBar1.Location = new System.Drawing.Point(253, 41);
            this.hScrollBar1.Maximum = 50;
            this.hScrollBar1.Name = "hScrollBar1";
            this.hScrollBar1.Size = new System.Drawing.Size(295, 13);
            this.hScrollBar1.TabIndex = 0;
            this.hScrollBar1.Value = 1;
            this.hScrollBar1.ValueChanged += new System.EventHandler(this.hScrollBar1_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Сглаживание";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // smoothingText
            // 
            this.smoothingText.AutoSize = true;
            this.smoothingText.Location = new System.Drawing.Point(94, 41);
            this.smoothingText.Name = "smoothingText";
            this.smoothingText.Size = new System.Drawing.Size(19, 13);
            this.smoothingText.TabIndex = 2;
            this.smoothingText.Text = "(0)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 190);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(183, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Логарифмическая шкала времени";
            // 
            // hScrollBar2
            // 
            this.hScrollBar2.LargeChange = 3;
            this.hScrollBar2.Location = new System.Drawing.Point(253, 190);
            this.hScrollBar2.Maximum = 1000;
            this.hScrollBar2.Name = "hScrollBar2";
            this.hScrollBar2.Size = new System.Drawing.Size(295, 13);
            this.hScrollBar2.TabIndex = 3;
            this.hScrollBar2.ValueChanged += new System.EventHandler(this.hScrollBar2_ValueChanged);
            // 
            // HistoryLengthText
            // 
            this.HistoryLengthText.AutoSize = true;
            this.HistoryLengthText.Location = new System.Drawing.Point(13, 119);
            this.HistoryLengthText.Name = "HistoryLengthText";
            this.HistoryLengthText.Size = new System.Drawing.Size(50, 13);
            this.HistoryLengthText.TabIndex = 6;
            this.HistoryLengthText.Text = "История";
            // 
            // hScrollBar3
            // 
            this.hScrollBar3.LargeChange = 1;
            this.hScrollBar3.Location = new System.Drawing.Point(253, 119);
            this.hScrollBar3.Maximum = 1440;
            this.hScrollBar3.Minimum = 1;
            this.hScrollBar3.Name = "hScrollBar3";
            this.hScrollBar3.Size = new System.Drawing.Size(295, 13);
            this.hScrollBar3.TabIndex = 5;
            this.hScrollBar3.Value = 30;
            this.hScrollBar3.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar3_Scroll);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(576, 313);
            this.tabControl1.TabIndex = 7;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.HistoryLengthText);
            this.tabPage1.Controls.Add(this.hScrollBar1);
            this.tabPage1.Controls.Add(this.hScrollBar3);
            this.tabPage1.Controls.Add(this.smoothingText);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.hScrollBar2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(568, 252);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Графики";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.smoothedWarnings);
            this.tabPage2.Controls.Add(this.notification_dust10_text);
            this.tabPage2.Controls.Add(this.notification_dust10_value);
            this.tabPage2.Controls.Add(this.notification_dust2_text);
            this.tabPage2.Controls.Add(this.notification_dust2_value);
            this.tabPage2.Controls.Add(this.notification_dust1_text);
            this.tabPage2.Controls.Add(this.notification_dust1_value);
            this.tabPage2.Controls.Add(this.notification_ch2o_text);
            this.tabPage2.Controls.Add(this.notification_ch2o_value);
            this.tabPage2.Controls.Add(this.notification_co2_text);
            this.tabPage2.Controls.Add(this.notification_co2_value);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(568, 287);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Уведомления";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // notification_dust10_text
            // 
            this.notification_dust10_text.AutoSize = true;
            this.notification_dust10_text.Location = new System.Drawing.Point(39, 204);
            this.notification_dust10_text.Name = "notification_dust10_text";
            this.notification_dust10_text.Size = new System.Drawing.Size(35, 13);
            this.notification_dust10_text.TabIndex = 9;
            this.notification_dust10_text.Text = "label3";
            // 
            // notification_dust10_value
            // 
            this.notification_dust10_value.Location = new System.Drawing.Point(216, 203);
            this.notification_dust10_value.Name = "notification_dust10_value";
            this.notification_dust10_value.Size = new System.Drawing.Size(302, 14);
            this.notification_dust10_value.TabIndex = 8;
            this.notification_dust10_value.Scroll += new System.Windows.Forms.ScrollEventHandler(this.notification_dust10_value_Scroll);
            // 
            // notification_dust2_text
            // 
            this.notification_dust2_text.AutoSize = true;
            this.notification_dust2_text.Location = new System.Drawing.Point(39, 159);
            this.notification_dust2_text.Name = "notification_dust2_text";
            this.notification_dust2_text.Size = new System.Drawing.Size(35, 13);
            this.notification_dust2_text.TabIndex = 7;
            this.notification_dust2_text.Text = "label3";
            // 
            // notification_dust2_value
            // 
            this.notification_dust2_value.Location = new System.Drawing.Point(216, 158);
            this.notification_dust2_value.Name = "notification_dust2_value";
            this.notification_dust2_value.Size = new System.Drawing.Size(302, 14);
            this.notification_dust2_value.TabIndex = 6;
            this.notification_dust2_value.Scroll += new System.Windows.Forms.ScrollEventHandler(this.notification_dust2_value_Scroll);
            // 
            // notification_dust1_text
            // 
            this.notification_dust1_text.AutoSize = true;
            this.notification_dust1_text.Location = new System.Drawing.Point(39, 115);
            this.notification_dust1_text.Name = "notification_dust1_text";
            this.notification_dust1_text.Size = new System.Drawing.Size(35, 13);
            this.notification_dust1_text.TabIndex = 5;
            this.notification_dust1_text.Text = "label3";
            // 
            // notification_dust1_value
            // 
            this.notification_dust1_value.Location = new System.Drawing.Point(216, 114);
            this.notification_dust1_value.Name = "notification_dust1_value";
            this.notification_dust1_value.Size = new System.Drawing.Size(302, 14);
            this.notification_dust1_value.TabIndex = 4;
            this.notification_dust1_value.Scroll += new System.Windows.Forms.ScrollEventHandler(this.notification_dust1_value_Scroll);
            // 
            // notification_ch2o_text
            // 
            this.notification_ch2o_text.AutoSize = true;
            this.notification_ch2o_text.Location = new System.Drawing.Point(39, 75);
            this.notification_ch2o_text.Name = "notification_ch2o_text";
            this.notification_ch2o_text.Size = new System.Drawing.Size(35, 13);
            this.notification_ch2o_text.TabIndex = 3;
            this.notification_ch2o_text.Text = "label3";
            // 
            // notification_ch2o_value
            // 
            this.notification_ch2o_value.Location = new System.Drawing.Point(216, 74);
            this.notification_ch2o_value.Maximum = 500;
            this.notification_ch2o_value.Name = "notification_ch2o_value";
            this.notification_ch2o_value.Size = new System.Drawing.Size(302, 14);
            this.notification_ch2o_value.TabIndex = 2;
            this.notification_ch2o_value.Scroll += new System.Windows.Forms.ScrollEventHandler(this.notification_ch2o_value_Scroll);
            // 
            // notification_co2_text
            // 
            this.notification_co2_text.AutoSize = true;
            this.notification_co2_text.Location = new System.Drawing.Point(39, 37);
            this.notification_co2_text.Name = "notification_co2_text";
            this.notification_co2_text.Size = new System.Drawing.Size(35, 13);
            this.notification_co2_text.TabIndex = 1;
            this.notification_co2_text.Text = "label3";
            // 
            // notification_co2_value
            // 
            this.notification_co2_value.Location = new System.Drawing.Point(216, 36);
            this.notification_co2_value.Maximum = 2000;
            this.notification_co2_value.Minimum = 400;
            this.notification_co2_value.Name = "notification_co2_value";
            this.notification_co2_value.Size = new System.Drawing.Size(302, 14);
            this.notification_co2_value.TabIndex = 0;
            this.notification_co2_value.Value = 400;
            this.notification_co2_value.Scroll += new System.Windows.Forms.ScrollEventHandler(this.notification_co2_value_Scroll);
            // 
            // smoothedWarnings
            // 
            this.smoothedWarnings.AutoSize = true;
            this.smoothedWarnings.Location = new System.Drawing.Point(42, 239);
            this.smoothedWarnings.Name = "smoothedWarnings";
            this.smoothedWarnings.Size = new System.Drawing.Size(222, 17);
            this.smoothedWarnings.TabIndex = 10;
            this.smoothedWarnings.Text = "Уведомления по сглаженным данным";
            this.smoothedWarnings.UseVisualStyleBackColor = true;
            this.smoothedWarnings.CheckedChanged += new System.EventHandler(this.smoothedWarnings_CheckedChanged);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 350);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "SettingsForm";
            this.Text = "SettingsForm";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.HScrollBar hScrollBar1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label smoothingText;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.HScrollBar hScrollBar2;
        private System.Windows.Forms.Label HistoryLengthText;
        private System.Windows.Forms.HScrollBar hScrollBar3;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label notification_dust10_text;
        private System.Windows.Forms.HScrollBar notification_dust10_value;
        private System.Windows.Forms.Label notification_dust2_text;
        private System.Windows.Forms.HScrollBar notification_dust2_value;
        private System.Windows.Forms.Label notification_dust1_text;
        private System.Windows.Forms.HScrollBar notification_dust1_value;
        private System.Windows.Forms.Label notification_ch2o_text;
        private System.Windows.Forms.HScrollBar notification_ch2o_value;
        private System.Windows.Forms.Label notification_co2_text;
        private System.Windows.Forms.HScrollBar notification_co2_value;
        private System.Windows.Forms.CheckBox smoothedWarnings;
    }
}