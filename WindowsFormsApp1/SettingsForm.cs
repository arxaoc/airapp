﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AirApp
{
    public partial class SettingsForm : Form
    {
        public Form1 m_mainForm;

        public SettingsForm()
        {
            InitializeComponent();
        }

        private static int Clamp(int v, int min, int max)
        {
            return Math.Max(Math.Min(v, max), min);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        public void Init()
        {
            hScrollBar1.Value = m_mainForm.Smoothing;
            hScrollBar2.Value = Clamp((int)(m_mainForm.Timescaler * timescaleMult), hScrollBar2.Minimum, hScrollBar2.Maximum);
            hScrollBar3.Value = Clamp(m_mainForm.HistoryLength, hScrollBar3.Minimum, hScrollBar3.Maximum);

            SetScrollValue(hScrollBar1, m_mainForm.Smoothing);
            SetScrollValue(hScrollBar2, (int)(m_mainForm.Timescaler * timescaleMult));
            SetScrollValue(hScrollBar3, m_mainForm.HistoryLength);

            SetScrollValue(notification_co2_value, m_mainForm.NotificationLevelCO2);
            SetScrollValue(notification_ch2o_value, m_mainForm.NotificationLevelCH2O);
            SetScrollValue(notification_dust1_value, m_mainForm.NotificationLevelDust1);
            SetScrollValue(notification_dust2_value, m_mainForm.NotificationLevelDust2);
            SetScrollValue(notification_dust10_value, m_mainForm.NotificationLevelDust10);

            smoothedWarnings.Checked = m_mainForm.SmoothedWarnings;

            UpdateNotificationsTexts();
            UpdateSmoothingText();
            UpdateHistoryLengthText();
        }

        private void UpdateNotificationsTexts()
        {
            notification_co2_text.Text = "CO2: " + notification_co2_value.Value;
            notification_ch2o_text.Text = "Формальдегид: " + ((float) notification_ch2o_value.Value / 1000).ToString("0.00");
            notification_dust1_text.Text = "Пыль 1мк: " + notification_dust1_value.Value;
            notification_dust2_text.Text = "Пыль 2мк: " + notification_dust2_value.Value;
            notification_dust10_text.Text = "Пыль 10мк: " + notification_dust10_value.Value;
        }

        private float timescaleMult = 100.0f;

        private void UpdateSmoothingText()
        {
            smoothingText.Text = string.Format("{0}", hScrollBar1.Value);
        }

        private void UpdateHistoryLengthText()
        {
            int hours = hScrollBar3.Value / 60;
            int minutes = hScrollBar3.Value - hours * 60;
            string text = "Временной интервал";
            if (minutes > 0 && hours > 0)
                HistoryLengthText.Text = string.Format("{0} {1}ч {2}м", text, hours,minutes);
            else if (hours > 0)
                HistoryLengthText.Text = string.Format("{0}  {1}ч", text, hours);
            else if(minutes > 0)
                HistoryLengthText.Text = string.Format("{0}  {1}м", text, minutes);
        }

        private void hScrollBar1_ValueChanged(object sender, EventArgs e)
        {
            m_mainForm.Smoothing = hScrollBar1.Value;
            UpdateSmoothingText();
        }

        private static void SetScrollValue(HScrollBar bar, int value )
        {
            bar.Value = Clamp(value, bar.Minimum, bar.Maximum);
        }

        private void hScrollBar2_ValueChanged(object sender, EventArgs e)
        {
            m_mainForm.Timescaler = (float)hScrollBar2.Value / timescaleMult;
        }

        private void hScrollBar3_Scroll(object sender, ScrollEventArgs e)
        {
            m_mainForm.HistoryLength = hScrollBar3.Value;
            UpdateHistoryLengthText();
        }

        private void notification_co2_value_Scroll(object sender, ScrollEventArgs e)
        {
            m_mainForm.NotificationLevelCO2 = Clamp(notification_co2_value.Value, 400, 2000);
            UpdateNotificationsTexts();
        }

        private void notification_ch2o_value_Scroll(object sender, ScrollEventArgs e)
        {
            m_mainForm.NotificationLevelCH2O = Clamp( notification_ch2o_value.Value, 0, 1000 );
            UpdateNotificationsTexts();
        }

        private void notification_dust1_value_Scroll(object sender, ScrollEventArgs e)
        {
            m_mainForm.NotificationLevelDust1 = notification_dust1_value.Value;
            UpdateNotificationsTexts();
        }

        private void notification_dust2_value_Scroll(object sender, ScrollEventArgs e)
        {
            m_mainForm.NotificationLevelDust2 = notification_dust2_value.Value;
            UpdateNotificationsTexts();
        }

        private void notification_dust10_value_Scroll(object sender, ScrollEventArgs e)
        {
            m_mainForm.NotificationLevelDust10 = notification_dust10_value.Value;
            UpdateNotificationsTexts();
        }

        private void smoothedWarnings_CheckedChanged(object sender, EventArgs e)
        {
            m_mainForm.SmoothedWarnings = smoothedWarnings.Checked;
        }
    }
}
