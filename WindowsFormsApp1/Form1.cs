﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using AirApp.Properties;

namespace AirApp
{
    public partial class Form1 : Form
    {
        UDPReceiver m_receiver;

        private class GetterDust1 : MDataGetter
        {
            public float GetValue(MonitorData d)
            {
                return d.Dust1;
            }
        }

        private class GetterDust2 : MDataGetter
        {
            public float GetValue(MonitorData d)
            {
                return d.Dust2;
            }
        }

        private class GetterDust10 : MDataGetter
        {
            public float GetValue(MonitorData d)
            {
                return d.Dust10;
            }
        }

        private class GetterCO2 : MDataGetter
        {
            public float GetValue(MonitorData d)
            {
                return d.CO2;
            }
        }

        private class GetterCH2O : MDataGetter
        {
            public float GetValue(MonitorData d)
            {
                return d.Formaldehyde;
            }
        }

 

        PlotDrawer m_drawer;

        public Form1()
        {
            InitializeComponent();
        }


        private List<MonitorData> m_datalist = new List<MonitorData>();

        private Dictionary<string, PlotData> m_plots = new Dictionary<string, PlotData>();

        private float margin = 0.007f;

        private Rectangle MargedRect(int rx, int ry, int rw, int rh)
        {
            int mx = (int)(rw * margin);
            int my = (int)(rh * margin);
            return new Rectangle(rx + mx, ry + my, rw - mx * 2, rh - mx * 2);
        }

        private void DrawCO2(Graphics gr, Bitmap bitmap, PlotData plotData)
        {
            m_drawer.m_getter = new GetterCO2();
            plotData.warningAxis.CO2 = NotificationLevelCO2;
            m_drawer.Start(plotData, gr, bitmap, MargedRect(0, 0, bitmap.Width / 2, bitmap.Height / 2), String.Format("CO2: {0}", plotData.last.CO2));
        }


        private void DrawCH2O(Graphics gr, Bitmap bitmap, PlotData plotData)
        {
            m_drawer.m_getter = new GetterCH2O();
            plotData.warningAxis.Formaldehyde = NotificationLevelCH2O / 1000.0f;
            m_drawer.Start(plotData, gr, bitmap, MargedRect(bitmap.Width / 2, 0, bitmap.Width / 2, bitmap.Height / 2), String.Format("Формальдегид: {0}", plotData.last.Formaldehyde.ToString("0.000")));
        }

        private void DrawDust1(Graphics gr, Bitmap bitmap, PlotData plotData)
        {
            Rectangle r = MargedRect(0, bitmap.Height / 2, bitmap.Width / 3, bitmap.Height / 2);

            m_drawer.m_getter = new GetterDust1();
            plotData.warningAxis.Dust1 = NotificationLevelDust1;
            m_drawer.Start(plotData, gr, bitmap, r, String.Format("1: {0}", plotData.last.Dust1));
        }

        private void DrawDust2(Graphics gr, Bitmap bitmap, PlotData plotData)
        {
            Rectangle r = MargedRect(bitmap.Width / 3, bitmap.Height / 2, bitmap.Width / 3, bitmap.Height / 2);

            m_drawer.m_getter = new GetterDust2();
            plotData.warningAxis.Dust2 = NotificationLevelDust2;
            m_drawer.Start(plotData, gr, bitmap, r, String.Format("2.5: {0}", plotData.last.Dust2));
        }

        private void DrawDust10(Graphics gr, Bitmap bitmap, PlotData plotData)
        {
            Rectangle r = MargedRect(2 * bitmap.Width / 3, bitmap.Height / 2, bitmap.Width / 3, bitmap.Height / 2);

            m_drawer.m_getter = new GetterDust10();
            plotData.warningAxis.Dust10 = NotificationLevelDust10;
            m_drawer.Start(plotData, gr, bitmap, r, String.Format("10: {0}", plotData.last.Dust10));
        }

        bool locked = false;

        public int Smoothing
        {
            get
            {
                return Properties.Settings.Default.Smoothing;
            }
            set
            {
                Properties.Settings.Default.Smoothing = value;
                Properties.Settings.Default.Save();
            }
        }

        public bool SmoothedWarnings
        {
            get
            {
                return Properties.Settings.Default.SmoothedWarnings;
            }
            set
            {
                Properties.Settings.Default.SmoothedWarnings = value;
                Properties.Settings.Default.Save();
            }
        }

        public float Timescaler
        {
            get
            {
                return Properties.Settings.Default.Timescaling;
            }
            set
            {
                Properties.Settings.Default.Timescaling = value;
                Properties.Settings.Default.Save();
            }
        }

        public int NotificationLevelCO2
        {
            get
            {
                return Properties.Settings.Default.CO2_WarningAt;
            }
            set
            {
                Properties.Settings.Default.CO2_WarningAt = value;
                Properties.Settings.Default.Save();
            }
        }

        public int NotificationLevelCH2O
        {
            get
            {
                return Properties.Settings.Default.CH20_WarningAt;
            }
            set
            {
                Properties.Settings.Default.CH20_WarningAt = value;
                Properties.Settings.Default.Save();
            }
        }

        public int NotificationLevelDust1
        {
            get
            {
                return Properties.Settings.Default.Dust1_WarningAt;
            }
            set
            {
                Properties.Settings.Default.Dust1_WarningAt = value;
                Properties.Settings.Default.Save();
            }
        }

        public int NotificationLevelDust2
        {
            get
            {
                return Properties.Settings.Default.Dust2_WarningAt;
            }
            set
            {
                Properties.Settings.Default.Dust2_WarningAt = value;
                Properties.Settings.Default.Save();
            }
        }

        public int NotificationLevelDust10
        {
            get
            {
                return Properties.Settings.Default.Dust10_WarningAt;
            }
            set
            {
                Properties.Settings.Default.Dust10_WarningAt = value;
                Properties.Settings.Default.Save();
            }
        }

        public int HistoryLength
        {
            get
            {
                return Properties.Settings.Default.HistoryLength;
            }
            set
            {
                Properties.Settings.Default.HistoryLength = value;
                Properties.Settings.Default.Save();
            }
        }

        private float SmoothedCO2;
        private float SmoothedCH2O;
        private float SmoothedDust1;
        private float SmoothedDust2;
        private float SmoothedDust10;

        private async void DrawAll()
        {
            if (pictureCO2.Width == 0 || pictureCO2.Height == 0)
                return;

            if (locked)
                return;

            locked = true;

            lock (m_datalist)
            {
                lock (m_plots)
                {
                    if (m_datalist != null && m_datalist.Count > 0)
                    {
                        PlotData plot;
                        if (!m_plots.TryGetValue(m_datalist[0].source, out plot))
                        {
                            plot = new PlotData();
                            m_plots.Add(m_datalist[0].source, plot);
                        }
                        plot.data = m_datalist;
                    }
                    
                }
            }

            if (m_drawer == null)
                m_drawer = new PlotDrawer();

            Bitmap bmp = new Bitmap(Math.Max(pictureCO2.Width, 5), Math.Max(pictureCO2.Height, 5));
            Graphics gr = Graphics.FromImage(bmp);

            m_drawer.Smoothing = Smoothing;
            m_drawer.scalingParameter = Timescaler;
            m_drawer.frame = new TimeSpan(0, HistoryLength, 0);

            Bitmap result = await Task.Factory.StartNew<Bitmap>(() =>
            {
                foreach (var p in m_plots.Values)
                {

                    DrawCO2(gr, bmp, p);
                    SmoothedCO2 = m_drawer.SmoothedLastValue;
                    DrawCH2O(gr, bmp, p);
                    SmoothedCH2O = m_drawer.SmoothedLastValue;
                    DrawDust1(gr, bmp, p);
                    SmoothedDust1 = m_drawer.SmoothedLastValue;
                    DrawDust2(gr, bmp, p);
                    SmoothedDust2 = m_drawer.SmoothedLastValue;
                    DrawDust10(gr, bmp, p);
                    SmoothedDust10 = m_drawer.SmoothedLastValue;
                }

                GC.Collect();
                GC.WaitForPendingFinalizers();

                return bmp;
            });

            gr.Dispose();

            pictureCO2.Image = bmp;

            locked = false;
        }

        public class Smoother
        {
            public float val = 0.0f;
            public float alpha;

            public Smoother(float a)
            {
                alpha = a;
            }

            public float Next(float x)
            {
                val = Next(val, x, alpha);
                return val;
            }
            
            public static float Next(float prev, float x, float a)
            {
                prev = x * a + (1.0f - a) * prev;
                return prev;
            }
        }

        public class AvgSmoother
        {
            private float n = 0.0f;
            public float result { get; private set; }

            public void Push(float val)
            {
                n += 1.0f;
                result = result * (1.0f - 1.0f / n) + val / n;
            }
        }

        private void OnMessage(string message)
        {
            lock (m_datalist)
            {
                MonitorData newrecord = new MonitorData(message);

                if (m_datalist.Count > 0)
                    m_datalist.Add(newrecord);
                else
                    m_datalist.Add(newrecord);

                while (DateTime.Now.Subtract(m_datalist[0].dateTime).Hours > 24)
                    m_datalist.RemoveAt(0);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            m_receiver = new UDPReceiver(6065);
            m_receiver.OnMessage = OnMessage;
            m_receiver.Start();
            timer1.Interval = 5000;
            timer1.Enabled = true;
            if (!IsRunningOnMono())
            {
                notifyIcon1.Icon = this.Icon;
                notifyIcon1.Visible = true;
            }
        }

        FormWindowState m_previousWindowState = FormWindowState.Normal;

        public static bool IsRunningOnMono()
        {
            return Type.GetType("Mono.Runtime") != null;
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            if (this.WindowState != FormWindowState.Minimized)
                m_previousWindowState = this.WindowState;
            if (!IsRunningOnMono())
            {
                switch (this.WindowState)
                {
                    case FormWindowState.Minimized:
                        notifyIcon1.Visible = true;
                        this.Hide();
                        break;
                    case FormWindowState.Normal:
                    case FormWindowState.Maximized:
                        //notifyIcon1.Visible = false;
                        break;
                }
            }
            DrawAll();

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Visible)
            {
                if (MessageBox.Show("Monitoring will stop. Do you want to close application anyway?", "Exit", MessageBoxButtons.YesNo) == DialogResult.No)
                    e.Cancel = true;
            }
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
        }

        private void pictureCO2_Click(object sender, EventArgs e)
        {

        }

        private DateTime m_lastBaloon;

        private void timer1_Tick_1(object sender, EventArgs e)
        {
            DrawAll();

            string message = "";
            lock (m_plots)
            {
                foreach (var k in m_plots)
                {
                    PlotData plotData = k.Value;
                    if ((SmoothedWarnings? SmoothedCO2 : plotData.last.CO2) > Properties.Settings.Default.CO2_WarningAt)
                        message += String.Format(k.Key + " CO2: {0}\n", plotData.last.CO2);

                    if ((SmoothedWarnings ? SmoothedCH2O : plotData.last.Formaldehyde) > Properties.Settings.Default.CH20_WarningAt)
                        message += String.Format(k.Key + " Формальдегид: {0}\n", plotData.last.Formaldehyde);

                    if ((SmoothedWarnings ? SmoothedDust1 : plotData.last.Dust1) > Properties.Settings.Default.Dust1_WarningAt)
                        message += String.Format(k.Key + " Пыль 1: {0}\n", plotData.last.Dust1);

                    if ((SmoothedWarnings ? SmoothedDust2 : plotData.last.Dust2) > Properties.Settings.Default.Dust2_WarningAt)
                        message += String.Format(k.Key + " Пыль 2.5: {0}\n", plotData.last.Dust2);

                    if ((SmoothedWarnings ? SmoothedDust10 : plotData.last.Dust10) > Properties.Settings.Default.Dust10_WarningAt)
                        message += String.Format(k.Key + " Пыль 10: {0}\n", plotData.last.Dust10);
                }
            }

            if (!string.IsNullOrEmpty(message))
            {
                if (DateTime.Now.Subtract(m_lastBaloon).TotalMinutes > 2)
                {
                    notifyIcon1.ShowBalloonTip(60000, "Воздух", message, ToolTipIcon.Warning);
                    m_lastBaloon = DateTime.Now;
                }
            }
        }

        private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = m_previousWindowState;
        }

        SettingsForm m_settingsForm;

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (m_settingsForm == null)
            {
                m_settingsForm = new SettingsForm();
                m_settingsForm.m_mainForm = this;
                m_settingsForm.Init();
                m_settingsForm.FormClosed += (s,a) => { m_settingsForm = null; };
                m_settingsForm.Show(this);
            }
        }
    }
}
