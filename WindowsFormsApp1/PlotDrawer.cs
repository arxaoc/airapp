﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AirApp
{
    class PlotDrawer
    {
        public enum State
        {
            Undefined,
            Initialized,
            Processing,
            Finished
        }

        public int Smoothing;

        public State state { get; private set; }

        public Mutex mutex = new Mutex();

        private Graphics m_graphics;

        private PlotData m_data;
        private Bitmap m_bitmap;
        public RectangleF m_frameRect;
        public RectangleF m_graphDataRect;
        public RectangleF m_widgetRect;
        public MDataGetter m_getter;

        private string m_title;

        public PlotDrawer()
        {
            state = State.Initialized;
        }

        public void Start(PlotData data, Graphics graphics, Bitmap bitmap, Rectangle rect, string title = "")
        {
            m_data = data;
            m_bitmap = bitmap;
            m_widgetRect = rect;
            m_frameRect = m_widgetRect;
            float titleheight = 20.0f;
            m_frameRect.Y += titleheight;
            m_frameRect.Height -= titleheight;
            m_graphDataRect = m_frameRect;
            m_graphDataRect.Width -= 5.0f;
            state = State.Processing;
            m_title = title;
            m_graphics = graphics;
            Run();
        }

        private float ToScreen(long value, long min, long max, float screenMin, float screenMax)
        {
            float screenSize = screenMax - screenMin;
            return screenMin + (int)((float)(value - min) / (max - min) * screenSize);
        }

        private float ToScreen(float value, float min, float max, float screenMin, float screenMax)
        {
            float screenSize = screenMax - screenMin;
            return (int)((value - min) / (max - min) * screenSize);
        }

        private float ToScreenY(float value, float min, float max, float screenMin, float screenMax)
        {
            return screenMax - ToScreen(value, min, max, screenMin, screenMax);
        }

        public TimeSpan frame;

        private float ToScreenTime(MonitorData d)
        {
            return ToScreenTime(d.dateTime.Ticks);
        }

        private float ToScreenTime(long ticks)
        {
            long max = rightPointTimeTicks;
            long min = rightPointTimeTicks - frame.Ticks;
            long mintomax = max - min;
            double fraq = (double)(max - ticks) / mintomax;
            double curfraq = curving(fraq);
            long curved = (long)(mintomax * curfraq);
            long mticks = max - curved;
            return ToScreen(mticks, min, max, m_graphDataRect.X, m_graphDataRect.X + m_graphDataRect.Width);
        }

        private float Clamp(float a, float min, float max)
        {
            return Math.Min(Math.Max(a, min), max);
        }

        private PointF Convert(int i)
        {
            float x = ToScreenTime(m_data.data[i]);
            float ymin = m_getter.GetValue(m_data.lowerBound);
            float ymax = m_getter.GetValue(m_data.upperBound);
            float yvalue = Clamp(m_getter.GetValue(m_data.data[i]), ymin, ymax);
             float y = ToScreenY(yvalue, ymin, ymax, m_graphDataRect.Y, m_graphDataRect.Y + m_graphDataRect.Height);
            return new PointF(x, y);
        }

        private float ConvertY(float yvalue)
        {
            float ymin = m_getter.GetValue(m_data.lowerBound);
            float ymax = m_getter.GetValue(m_data.upperBound);
            return ToScreenY(yvalue, ymin, ymax, m_graphDataRect.Y, m_graphDataRect.Y + m_graphDataRect.Height);
        }

        float[] dashValues = { 5, 5, 5, 5, 5, 5, 15, 5, 15, 5, 15, 5, 5, 5, 5, 5, 5, 10 };

        private void HorizontalAxis(Graphics g, float y, Color col, float width = 5, bool dashed = false)
        {
            Pen grayPen = new Pen(col, width);
            if(dashed)
                grayPen.DashPattern = dashValues;

            float py = ConvertY(y);
            g.DrawLine(grayPen, new PointF(m_frameRect.X, py),  new PointF(m_frameRect.X + m_frameRect.Width, py));
        }

        private DateTime rightPointTime
        {
            get
            {
                return DateTime.Now;
            }
        }

        private long rightPointTimeTicks
        {
            get
            {
                return rightPointTime.Ticks;
            }
        }

        private void VerticalAxis(Graphics g, DateTime time, Color col)
        {
            Pen grayPen = new Pen(col, 1);
            float x = ToScreenTime(time.Ticks);
            g.DrawLine(grayPen, new PointF(x, m_graphDataRect.Y), new PointF(x, m_graphDataRect.Y + m_graphDataRect.Height));
        }

        private void DrawFrame(Graphics g)
        {
            Pen pen = new Pen(Color.Black);
            g.DrawRectangle(pen, new Rectangle((int)m_frameRect.X, (int)m_frameRect.Y, (int)m_frameRect.Width, (int)m_frameRect.Height));

            Font font = new Font("Courier New", 12, FontStyle.Regular);
            g.DrawString(m_title, font, Brushes.Black, m_widgetRect.X, m_widgetRect.Y);

        }


        public class FloatingAvg
        {
            float sum = 0.0f;
            float[] previous;
            int count = 0;
            int index = -1;

            public FloatingAvg(int n)
            {
                previous = new float[n];
            }

            public float Average
            {
                get
                {
                    return sum / count;
                }
            }

            public void Push(float x)
            {
                if (count < previous.Length)
                {
                    ++count;
                    ++index;
                }
                else
                {
                    index = (index + 1) % count;
                }
                sum -= previous[index];
                previous[index] = x;
                sum += x;
            }
        }

        private double m_scalingParameter;
        private double m_scalingParameter2;

        public double scalingParameter
        {
            get
            {
                return m_scalingParameter;
            }
            set
            {
                m_scalingParameter = value;
                m_scalingParameter2 = 1.0 / (Math.Exp(-m_scalingParameter) - 1.0);
            }
        }

        private const int scalingnomalizer = 100000000;

        private double curving(double x)
        {
            if (Math.Abs(scalingParameter) < double.Epsilon)
                return x;
            return m_scalingParameter2 * (Math.Exp(-m_scalingParameter * x) - 1.0);
        }

        private long TimeScale(long ticksCount)
        {
            ticksCount /= scalingnomalizer;

            //return ticksCount;
            return (long)(scalingParameter * Math.Log((float)ticksCount / scalingParameter + 1.0f)) * scalingnomalizer;
        }

        private long TimeInverseScale(long ticksCount)
        {
            ticksCount /= scalingnomalizer;

            double exp = Math.Exp(ticksCount / scalingParameter);

            //return ticksCount;
            return (long)((exp - 1.0f) * scalingParameter) * scalingnomalizer;
        }

        public float SmoothedLastValue { get; private set; }

        private float GetValue(int i)
        {
            return m_getter.GetValue(m_data.data[i]);
        }

        public void Run()
        {
            List<MonitorData> data = m_data.data;
            if (data.Count > 0)
            {
                Graphics g = m_graphics;

                HorizontalAxis(g, m_getter.GetValue(m_data.greenAxis), Color.Orange);
                HorizontalAxis(g, m_getter.GetValue(m_data.redAxis), Color.LightCoral);

                HorizontalAxis(g, m_getter.GetValue(m_data.warningAxis), Color.Gray, 1, true);

                for (int i = 1; i < 24; i += 1)
                {
                    DateTime t = DateTime.Now - new TimeSpan(i, 0, 0);
                    float screenf = ToScreenTime(t.Ticks);
                    if (m_frameRect.Left < screenf && screenf < m_frameRect.Right)
                        VerticalAxis(g, t, Color.Gray);
                    else
                        break;
                }

                for (int i = 0; i < 60; i += 10)
                {
                    DateTime t = DateTime.Now - new TimeSpan(0, i, 0);
                    float screenf = ToScreenTime(t.Ticks);
                    if (m_frameRect.Left < screenf && screenf < m_frameRect.Right)
                        VerticalAxis(g, t, Color.LightGray);
                    else
                        break;
                }

                DrawFrame(g);

                Pen pen = new Pen(Color.Blue,1);
                long now = rightPointTimeTicks;
                long tooFar = now - frame.Ticks;

                FloatingAvg avg = new FloatingAvg(Smoothing + 1);

                FloatingAvg sourceValues = new FloatingAvg(Smoothing + 1);

                int iStart = data.Count - 1;
                for (; iStart > 0; --iStart)
                {
                    if (data[iStart].dateTime.Ticks < tooFar)
                        break;
                }

                for (int i = 0; i < Smoothing; ++i)
                {
                    int j = iStart - i - 1;
                    if (j >= 0)
                    {
                        sourceValues.Push(GetValue(i));
                        avg.Push(Convert(j).Y);
                    }
                }

                PointF? prevPoint = null;
                for (int i = iStart; i < data.Count; ++i)
                {
                    if (!prevPoint.HasValue)
                    {
                        prevPoint = Convert(i);
                        sourceValues.Push(GetValue(i));
                        avg.Push(prevPoint.Value.Y);
                    }
                    else
                    {
                        sourceValues.Push(GetValue(i));
                        PointF nextPoint = Convert(i);

                        avg.Push(nextPoint.Y);

                        nextPoint.Y = avg.Average;

                        if (prevPoint.Value.X > m_frameRect.X && prevPoint.Value.X < m_frameRect.X + m_frameRect.Width)
                            g.DrawLine(pen, prevPoint.Value, nextPoint);
                        prevPoint = nextPoint;
                    }
                }

                SmoothedLastValue = sourceValues.Average;
            }

            state = State.Finished;
        }
    }
}
