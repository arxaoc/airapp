﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirApp
{
    public struct MonitorData
    {

        public static int Find(string s, string subst, int start = 0)
        {
            int i = start;
            for (; i < s.Length - subst.Length; ++i)
            {
                int j = 0;
                for (; j < subst.Length; ++j)
                {
                    if (s[i + j] != subst[j])
                        break;
                }
                if (j == subst.Length)
                    break;
            }
            return i;
        }

        private static string ExtractRecord(string s, string head, string tail)
        {
            int i = Find(s, head);
            int rStart = i + head.Length;
            int j = Find(s, tail, rStart);
            return s.Substring(rStart, j - rStart);
        }

        private static float ExtractRecordFloat(string s, string head, string tail)
        {
            float result;
            if (float.TryParse(ExtractRecord(s, head, tail), out result))
                return result;
            return 0;
        }

        private static int ExtractRecordInt(string s, string head, string tail)
        {
            int result;
            if (int.TryParse(ExtractRecord(s, head, tail), out result))
                return result;
            return 0;
        }




        public MonitorData(string s)
        {
            dateTime = DateTime.Now;
            CO2 = ExtractRecordInt(s, "*S3=", "*");
            Formaldehyde = ExtractRecordFloat(s, "*S9=", "*") / 1000.0f;
            Dust1 = ExtractRecordInt(s, "*S2=", "*");
            Dust2 = ExtractRecordInt(s, "*S6=", "*");
            Dust10 = ExtractRecordInt(s, "*S7=", "*");
            source = ExtractRecord(s, "*S4=", "*");
        }

        public DateTime dateTime;
        public int CO2;
        public float Formaldehyde;
        public int Dust1;
        public int Dust2;
        public int Dust10;
        public string source;

        //public int Dust03Count;
        //public int Dust05Count;
        //public int Dust1Count;
        //public int Dust2Count;
        //public int Dust5Count;
        //public int Duat10Count;
    }


    public interface MDataGetter
    {
        float GetValue(MonitorData d);
    }

}
