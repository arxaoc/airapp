﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirApp
{
    public class PlotData
    {
        /// <summary>
        /// Sorted by time
        /// </summary>
        public List<MonitorData> data;

        public MonitorData lowerBound;
        public MonitorData upperBound;

        public MonitorData greenAxis;
        public MonitorData redAxis;

        public MonitorData warningAxis;

        public MonitorData last
        {
            get
            {
                if (data != null && data.Count > 0)
                    return data[data.Count - 1];
                return new MonitorData();
            }
        }

        public int iStart;
        public int iEnd;

        public PlotData()
        {
            Autobounds();
        }

        public int count
        {
            get
            {
                return data.Count;
            }
        }

        public void Autobounds()
        {
            lowerBound = new MonitorData();
            lowerBound.CO2 = 300;
            lowerBound.dateTime = DateTime.Now - (new TimeSpan(0, 30, 0));
            lowerBound.Dust1 = 0;
            lowerBound.Dust2 = 0;
            lowerBound.Dust10 = 0;
            lowerBound.Formaldehyde = 0;

            upperBound = new MonitorData();
            upperBound.CO2 = 1500;
            upperBound.dateTime = DateTime.Now;
            upperBound.Dust1 = 100;
            upperBound.Dust2 = 100;
            upperBound.Dust10 = 100;
            upperBound.Formaldehyde = 0.2f;

            greenAxis = new MonitorData();
            greenAxis.CO2 = 800;
            greenAxis.dateTime = DateTime.Now;
            greenAxis.Dust1 = 20;
            greenAxis.Dust2 = 30;
            greenAxis.Dust10 = 50;
            greenAxis.Formaldehyde = 0.01f;

            redAxis = new MonitorData();
            redAxis.CO2 = 1200;
            redAxis.dateTime = DateTime.Now;
            redAxis.Dust1 = 60;
            redAxis.Dust2 = 70;
            redAxis.Dust10 = 80;
            redAxis.Formaldehyde = 0.1f;

            warningAxis = new MonitorData();
            warningAxis.CO2 = 1200;
            warningAxis.dateTime = DateTime.Now;
            warningAxis.Dust1 = 60;
            warningAxis.Dust2 = 70;
            warningAxis.Dust10 = 80;
            warningAxis.Formaldehyde = 0.1f;
        }
    }


}
