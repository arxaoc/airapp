﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AirApp
{

    class UDPReceiver
    {
        IPEndPoint m_endPoint;

        public UDPReceiver(int port)
        {
            m_endPoint = new IPEndPoint(IPAddress.Any, port);
        }

        public void Start()
        {
            if (m_onReceive != null)
            {
                throw new Exception("Already started, stop first");
            }
            m_connection = new UdpClient();
            //m_connection.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            m_connection.Client.Bind(m_endPoint);
            m_connection.EnableBroadcast = true;
            Console.WriteLine("Started listening");
            StartListening();
        }
        public void Stop()
        {
            try
            {
                m_connection.Close();
                Console.WriteLine("Stopped listening");
            }
            catch { /* don't care */ }
        }

        private UdpClient m_connection;
        IAsyncResult m_onReceive = null;

        public Action<String> OnMessage;

        private void StartListening()
        {
            m_onReceive = m_connection.BeginReceive(Receive, new object());
        }
        private void Receive(IAsyncResult ar)
        {
            byte[] bytes = m_connection.EndReceive(ar, ref m_endPoint);
            string message = Encoding.ASCII.GetString(bytes);
            Console.WriteLine("From {0} received: {1} ", m_endPoint.Address.ToString(), message);
            if (OnMessage != null)
                OnMessage(message);
            StartListening();
        }
    }

}
